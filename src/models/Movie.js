const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de mascotas
const movieSchema = new Schema(
  {
    title: { type: String, required: true },
    plot: { type: String},
    year: { type: Date },
    director: { type: String},
    genre: { type: String},
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Pet
const Movie = mongoose.model('Movie', movieSchema);
module.exports = Movie;