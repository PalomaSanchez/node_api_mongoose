const express = require("express");
const movierouter = require("./routes/movie.routes");

require("./db.js");

const PORT = 3000;
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.use("/movies", movierouter);


server.use('*', (req, res, next) => {
  const error = new Error('Route not found'); 
  error.status = 404;
  next(error); 
});

server.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || 'Unexpected error');
});



server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});