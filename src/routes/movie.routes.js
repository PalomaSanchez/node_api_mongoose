const express = require("express");

const Movie = require("../models/Movie");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const pets = await Movie.find();
    return res.status(200).json(pets);

  } catch (err) {
    return res.status(500).json(err);

  }
});

router.post("/", async (req, res, next) => {
  try {
    const newMovie = new Movie({
      title: req.body.title,
      plot: req.body.plot,
      year: new Date(req.body.year),
      director: req.body.director,
      genre: req.body.genre,
    });

    const createdMovie = await newMovie.save();
    return res.status(200).json(createdMovie);

  } catch (err) {
    next(err);
  }
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;

  try {
    const movie = await Movie.findById(id);

    if (movie) {
      return res.status(200).json(movie);
    } else {
      return res.status(404).json({ mensaje: "No movie found by this id" });
    }
  } catch (err) {
    next(err);
  }
});

router.put("/", async (req, res, next) => {
  try {
    const id = req.body.id;

    const updatedMovie = await Movie.findByIdAndUpdate(
      id, 
      { $set: req.body }, 
      { new: true } 
    );

    return res.status(200).json(updatedMovie);

  } catch (err) {
    next(err);
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;

    await Movie.findByIdAndDelete(id);
    return res.status(200).json({ message: "Movie deleted!" });

  } catch (err) {
    next(err);
  }
});

router.get('/year/:year', async (req, res, next) => {   
    try {
       const year = req.params.year;
       const MoviesByYear = await Movie.find({ year: { $lt: year } });
       return res.status(200).json(MoviesByYear);
       
   } catch (err) {
       next(err);
   }
 });

 router.get('/genre/:genre', async (req, res, next) => {   
    try {
       const genre = req.params.genre;
       const busqueda =  "/" + genre + "/";
       const MoviesByGenre = await Movie.find({ genre: { $regex: genre } });
       if(MoviesByGenre.length>0){
        return res.status(200).json(MoviesByGenre);
       }else{
        const error = new Error('Films not found'); 
        error.status = 404;
        next(error);
       }
       
       
   } catch (err) {
       next(err);
   }
 });


module.exports = router;
