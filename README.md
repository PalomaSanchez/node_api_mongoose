# Node_API

NODE API with Mongoose conection.

## Routes
Get all movies with get:
```
/movies
```
Create new movie with post:
```
/movies
```
post body to create:
```
{
    "title":"Title of the movie",
    "plot":"Synopsis",
    "year":"2020",
    "director":"Director",
    "genre":"Genre"
}
```
Get movie by id:
```
/movies/[id]
```
Update a movie with id:
```
/movies
```
put body to create:
```
{
    "id":"5f22ac70b756b6293c73c878"
    "title":"Tilete of the movie",
    "plot":"Synopsis",
    "year":"2020",
    "director":"Director",
    "genre":"Genre"
}
```
Delete movie by id with Delete:
```
/movies/[id]
```
Get movie's by genre:
```
/movies/[genre]
```
Get movie's that year is lower than parameter passed:
```
/movies/[year]
```

### Installing
Install all the modules

```
npm install
```

For dev run use:

```
npm run dev
```
 

## Authors

* **Paloma Sánchez** - *Initial work* - [@PalomaSanchez](https://gitlab.com/PalomaSanchez)

